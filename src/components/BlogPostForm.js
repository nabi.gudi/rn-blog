import React, { useState }from 'react';
import { View, Text, StyleSheet, TextInput, Button} from 'react-native';

const BlogPostForm = ( { navigation, route, onSubmit, initialValues } ) => {
  const [title, setTitle] = useState(initialValues.title);
  const [content, setContent] = useState(initialValues.content);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Title: </Text>
      <TextInput 
        style={styles.input}
        value={title}
        onChangeText={text => setTitle(text)}
      />
      <Text style={styles.title}>Content:</Text>
      <TextInput 
        style={styles.input}
        value={content}
        onChangeText={text => setContent(text)}
      />
      <Button
        title={'Save blog post'} 
        onPress={() => { 
          onSubmit(title, content)}}
      />
    </View>
  )
}

BlogPostForm.defaultProps = {
  initialValues: {
    title: '',
    content: ''
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15
  },
  title:{
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10
  },
  content:{
    fontSize: 18
  },
  input: {
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 4, 
    padding: 10,
    fontSize: 16,
    marginBottom: 10
  }
});

export default BlogPostForm;