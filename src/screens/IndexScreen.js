import React, { useContext } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import { Context } from '../context/BlogContext';
import { FontAwesome } from '@expo/vector-icons';

const IndexScreen = ( { navigation } ) => {
  const { state, deleteBlogPost } = useContext(Context);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={styles.plus} onPress={() => navigation.navigate('Create')}>
          <FontAwesome name="plus" size={25} />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  return (
    <View style={styles.background}>
      <FlatList
        data={state}
        style={styles.list}
        keyExtractor={(post) => post.id}
        renderItem={ ( {item} ) => {
            return(
              <TouchableOpacity
                onPress={() => navigation.navigate('Show', { id: item.id })}
              >
                <View style={styles.row}>
                  <Text style={styles.text}>{item.title}</Text>
                  <TouchableOpacity
                    onPress={() => deleteBlogPost(item.id)}
                  >
                    <FontAwesome name="trash-o" style={styles.icon}/>
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            )
          }
        }
      />
    </View>
  )
};

const styles = StyleSheet.create({
  list: {
    borderBottomWidth: 1,
    borderColor: 'gray'
  },
  row:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    paddingHorizontal: 10,
    borderTopWidth: 1,
    borderColor: 'gray'
  },
  text:{
    fontSize: 18
  },
  icon: {
    fontSize: 25
  },
  plus:{
    marginRight: 15
  }
});

export default IndexScreen;