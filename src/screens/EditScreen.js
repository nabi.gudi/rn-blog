import React, { useContext } from 'react';
import {StyleSheet} from 'react-native';
import { Context } from '../context/BlogContext';
import BlogPostForm from '../components/BlogPostForm';

const EditScreen = ( { navigation, route } ) => {
  const { id } = route.params;

  const { state, editBlogPost } = useContext(Context);

  const blogPost = state.find((blogPost) => blogPost.id === id);

  return <BlogPostForm 
    initialValues={{title: blogPost.title, content: blogPost.content}}
    onSubmit={(title, content) => { editBlogPost(id, title, content, () => navigation.pop()) }}
  />

}

const styles = StyleSheet.create({
  container: {
    padding: 15
  },
  title:{
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10
  },
  content:{
    fontSize: 18
  },
  input: {
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 4, 
    padding: 10,
    fontSize: 16,
    marginBottom: 10
  }
});

export default EditScreen;