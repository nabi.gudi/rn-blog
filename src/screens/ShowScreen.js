import React, { useContext } from 'react';
import { View, Text, StyleSheet,TouchableOpacity} from 'react-native';
import { Context } from '../context/BlogContext';
import { FontAwesome } from '@expo/vector-icons';

const ShowScreen = ( { navigation, route } ) => {
  const { id } = route.params;

  const { state } = useContext( Context );

  const blogPost = state.find((blogPost) => blogPost.id === id);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={styles.edit} onPress={() => navigation.navigate('Edit', {id})}>
          <FontAwesome name="pencil" size={25} />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{blogPost.title}</Text>
      <Text style={styles.content}>{blogPost.content}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 15
  },
  title:{
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10
  },
  content:{
    fontSize: 18
  },
  edit:{
    marginRight: 15
  }
});

export default ShowScreen;